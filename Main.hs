{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TupleSections #-}

module Main (main) where

import Game
import IO (gameLoop)

import Control.Category (Category (..))
import Data.Bool (bool)
import Data.List (sortOn)
import Prelude hiding ((.), id)

value :: Board -> Int
value b = case won b of
    Just (Won Cross)  -> 1
    Just (Won Naught) -> -1
    _                 -> 0

minimax :: (Ord b)
        => (a -> b) -- ^ Evaluation function
        -> Bool     -- ^ Are we maximising?
        -> Tree a   -- ^ Game tree
        -> b        -- ^ Evaluation
minimax val p (Tree b cs) = hope
  where
    cs' = minimax val (not p) <$> cs
    opt = bool minimum maximum p
    hope | [] <- cs  = val b
         | otherwise = opt cs'

insertThenPlay :: (Idx, Idx) -> Board -> Maybe Board
insertThenPlay coords b = maybePlay <$> insert Cross coords b
  where
    maybePlay b'
      | Tree _ cs@(_:_) <- gameTree Naught b' = label . head $ sortOn (minimax value False) cs
      | otherwise                             = b'

main :: IO ()
main = gameLoop showBoard resMsg won insertThenPlay empty
  where
    resMsg Draw    = "It was a draw..."
    resMsg (Won w) = show w ++ " won!"
