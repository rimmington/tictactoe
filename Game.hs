{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}

module Game (
    Board, Idx (..), Player (..), Result (..), Tree (..)
  , empty, gameTree, insert, showBoard, won
  ) where

import Control.Category (Category (..))
import Data.Bool (bool)
import Data.List (intercalate)
import Data.Maybe (mapMaybe)
import Data.Monoid (First (..))
import Data.Tuple (swap)
import Prelude hiding ((.), id)

data Player = Naught | Cross deriving (Eq, Show)
type Square = Maybe Player
newtype Board = Board [Square]
data Result = Won Player | Draw deriving (Eq, Show)

data Idx = I | II | III deriving (Eq, Show, Ord, Enum, Bounded)

instance Read Idx where
    readsPrec i s = map swap . traverse try . swap =<< readsPrec i s
      where
        try 0 = [I]
        try 1 = [II]
        try 2 = [III]
        try _ = []

universe :: (Bounded a, Enum a) => [a]
universe = [minBound..maxBound]

-- | An empty board.
empty :: Board
empty = Board $ replicate 9 Nothing

coord :: (Idx, Idx) -> Int
coord (x, y) = fromEnum x * 3 + fromEnum y

get :: Board -> (Idx, Idx) -> Square
get (Board ss) = (ss !!) . coord

set :: Board -> (Idx, Idx) -> Square -> Board
set (Board ss) c s = Board $ h ++ [s] ++ tail t
  where
    (h, t) = splitAt (coord c) ss

row :: Idx -> Board -> [Square]
row i (Board ss) = take 3 $ drop (fromEnum i * 3) ss

col :: Idx -> Board -> [Square]
col i b = [get b (x, i) | x <- universe]

diagL, diagR :: Board -> [Square]
diagL b = get b <$> [(I, I), (II, II), (III, III)]
diagR b = get b <$> [(I, III), (II, II), (III, I)]

-- | Has someone won?
won :: Board -> Maybe Result
won b = first [ try (row I), try (row II), try (row III)
              , try (col I), try (col II), try (col III)
              , try diagL, try diagR
              , draw ]
  where
    try f
      | all (== Just Naught) ss = Just $ Won Naught
      | all (== Just Cross) ss  = Just $ Won Cross
      | otherwise               = Nothing
      where ss = f b
    draw
      | (Board ss) <- b, all (/= Nothing) ss = Just Draw
      | otherwise                            = Nothing
    first = getFirst . mconcat . map First

-- | Attempt to mark the square at coordinates. Returns Nothing for an invalid
-- move, crashes (!) if coordinates are invalid.
insert :: Player -> (Idx, Idx) -> Board -> Maybe Board
insert sq c b
  | Nothing <- get b c = Just (set b c $ Just sq)
  | otherwise          = Nothing

data Tree a = Tree { label :: a, _children :: [Tree a] } deriving (Eq, Show)

-- | Tree of valid plays from this board, given it's some player's turn.
gameTree :: Player -> Board -> Tree Board
gameTree p b = Tree b $ gameTree (other p) <$> mapMaybe (flip (insert p) b) [(x, y) | x <- universe, y <- universe]

-- | The other player.
other :: Player -> Player
other Cross  = Naught
other Naught = Cross

showBoard :: Board -> String
showBoard (Board [v1, v2, v3, v4, v5, v6, v7, v8, v9]) =
    '┌' : '─' : '─'  : '─' : '┬' : '─' : '─'  : '─' : '┬' : '─' : '─'  : '─' : '┐' : '\n' :
    '│' : ' ' : v v1 : ' ' : '│' : ' ' : v v2 : ' ' : '│' : ' ' : v v3 : ' ' : '│' : '\n' :
    '├' : '─' : '─'  : '─' : '┼' : '─' : '─'  : '─' : '┼' : '─' : '─'  : '─' : '┤' : '\n' :
    '│' : ' ' : v v4 : ' ' : '│' : ' ' : v v5 : ' ' : '│' : ' ' : v v6 : ' ' : '│' : '\n' :
    '├' : '─' : '─'  : '─' : '┼' : '─' : '─'  : '─' : '┼' : '─' : '─'  : '─' : '┤' : '\n' :
    '│' : ' ' : v v7 : ' ' : '│' : ' ' : v v8 : ' ' : '│' : ' ' : v v9 : ' ' : '│' : '\n' :
    '└' : '─' : '─'  : '─' : '┴' : '─' : '─'  : '─' : '┴' : '─' : '─'  : '─' : '┘' : []
  where
    v (Just Cross)  = 'X'
    v (Just Naught) = 'O'
    v Nothing       = ' '
showBoard _ = error "Bad board"
